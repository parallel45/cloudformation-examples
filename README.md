

## Slides

<https://slides.com/yvesgurcan/cloudformation>

## Templates

- Hello Bucket

A simple template that create an S3 bucket configured to serve webpages.

- Lambda/Dynamo

A template that pairs a Lambda function and a DynamoDB table.

- API built with Stackery

A template and repository with an API Gateway, a Lambda, and a DynamoDB table. Built programmatically with Stackery.

<https://github.com/yvesgurcan/stackery-api-example>
